package org.opd;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String mode = "";

        while (!mode.equals("a") && !mode.equals("b")) {
            Scanner chooseModeScanner = new Scanner(System.in);
            System.out.println("What do you want to convert :\n - (a) Text to Shadok\n - (b) Shadok to Text");
            mode = chooseModeScanner.nextLine();
        }

        if (mode.equals("a")) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter your text : ");
            System.out.println("\nThis is your text in Shadok : " + textToShadok(scanner.nextLine()));
        }

        if (mode.equals("b")) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter your Shadok text (format : BU-MEU-ZO-GA BU-MEU-ZO-GA): ");
            String originalText = shadokToText(scanner.nextLine());
            if (originalText.equals("")) {
                System.err.println("\nERROR : MALFORMED TEXT !");
            } else {
                System.out.println("\nThis is your original text : " + shadokToText(scanner.nextLine()));
            }
        }
    }

    private static String shadokToText(String shadok) {

        shadok = shadok.toUpperCase();
        String[] wordsInShadok = shadok.split(" ");

        int maxLength = 0;
        for (String words : wordsInShadok) {
            if (maxLength < words.length()) {
                maxLength = words.length();
            }
        }

        String[][] charactersInShadok = new String[wordsInShadok.length][maxLength];

        for (int i = 0; i < wordsInShadok.length; i++) {
            String[] charInShadok = wordsInShadok[i].split("-");
            System.arraycopy(charInShadok, 0, charactersInShadok[i], 0, charInShadok.length);
        }

        StringBuilder shadokToNumber = new StringBuilder();

        for (String[] word : charactersInShadok) {
            for (String character : word) {
                if (character != null) {
                    shadokToNumber.append(wordToNumber(character));
                }
            }
            shadokToNumber.append(" ");
        }

        String[] numberToChar = shadokToNumber.toString().split(" ");

        StringBuilder finalText = new StringBuilder();

        for (String s : numberToChar) {
            char[] chars = Character.toChars(base4ToBase10(s));
            finalText.append(chars);
        }
        return finalText.toString();
    }

    public static String textToShadok(String userInput) {
        String[] characters = textToNumber(userInput).split(" ");

        StringBuilder shadokText = new StringBuilder();

        for (String character : characters) {

            String numberModulo = combineModulo(character);

            String numberModuloReversed = reverseString(numberModulo);

            shadokText.append(numberToWord(numberModuloReversed));
            shadokText.append(" ");
        }

        return shadokText.toString();
    }

    public static int base4ToBase10(String base4) {
        int charCount = base4.length();
        int total = 0;
        String[] base4Split = base4.split("");
        for (String number : base4Split) {
            charCount--;
            int pow = (int) (Integer.parseInt(number) * Math.pow(4, charCount));
            total += pow;
        }
        return total;
    }

    public static String textToNumber(String text) {
        char[] chars = text.toCharArray();
        StringBuilder numbers = new StringBuilder();
        for (char character : chars) {
            numbers.append((int) character);
            numbers.append(" ");
        }
        return numbers.toString();
    }

    public static String numberToWord(String number) {
        //0 -> GA, 1 -> BU, 2 -> ZO, 3 -> MEU
        String[] digits = number.split("");
        StringBuilder textResult = new StringBuilder();
        switch (digits[0]) {
            case "0" -> textResult.append("GA");
            case "1" -> textResult.append("BU");
            case "2" -> textResult.append("ZO");
            case "3" -> textResult.append("MEU");
        }
        for (int i = 1; i < digits.length; i++) {
            textResult.append("-");
            switch (digits[i]) {
                case "0" -> textResult.append("GA");
                case "1" -> textResult.append("BU");
                case "2" -> textResult.append("ZO");
                case "3" -> textResult.append("MEU");
            }
        }
        return textResult.toString();
    }

    public static String wordToNumber(String word) {
        //0 -> GA, 1 -> BU, 2 -> ZO, 3 -> MEU
        StringBuilder numberResult = new StringBuilder();
        switch (word) {
            case "GA" -> numberResult.append("0");
            case "BU" -> numberResult.append("1");
            case "ZO" -> numberResult.append("2");
            case "MEU" -> numberResult.append("3");
        }
        return numberResult.toString();
    }

    public static String combineModulo(String stringNumber) {
        StringBuilder numberModulo = new StringBuilder();
        int divisionResult = Integer.parseInt(stringNumber);
        if (divisionResult == 0) {
            numberModulo = new StringBuilder("0");
        }
        while (divisionResult != 0) {
            numberModulo.append(divisionResult % 4);
            divisionResult = divisionResult / 4;
        }
        return numberModulo.toString();
    }

    public static String reverseString(String str) {
        StringBuilder toReverse = new StringBuilder(str);
        toReverse.reverse();
        return toReverse.toString();
    }
}
